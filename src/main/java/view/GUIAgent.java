package view;
/**
 * Created by Radek on 2017-12-16.
 */

import agents.User;
import agents.utils.*;
import blockchain.Block;
import blockchain.Serializer;
import blockchain.blocks.BookClassroom;
import blockchain.blocks.CancelBooking;
import blockchain.filters.*;
import jade.core.AID;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import misc.Logger;
import misc.WsdUtils;
import view.builders.ClassroomDataTableViewBuilder;
import view.builders.MyBookingsTableViewBuilder;
import view.dao.ClassroomData;
import view.dao.MyBookingsData;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.function.Predicate;

import static jade.lang.acl.MessageTemplate.MatchPerformative;
import static jade.lang.acl.MessageTemplate.MatchProtocol;

public class GUIAgent extends User implements IGUIRunnable {

    @FXML
    TableView classroomDataTableView;

    @FXML
    DatePicker bookingDatePicker;

    @FXML
    ChoiceBox<String> bookingHourChoiceBox;

    @FXML
    ChoiceBox<String> bookingDurationChoiceBox;

    @FXML
    TextArea messageTextArea;

    @FXML
    Label hoursLabel;

    @FXML
    TableView myBookingsTableView;

    public GUIAgent() throws NoSuchAlgorithmException, NoSuchMethodException {
        super();
    }

    @Override
    protected void setup() {
        super.setup();
        new ApplicationLauncherThread(this).start();
        try {
            this.addNewMessageProcessor(MessageTemplate.and(
                    MatchPerformative(ACLMessage.ACCEPT_PROPOSAL),
                    MatchProtocol(Protocol.BOOKING_CHANGE)),
                    "changeAccepted"
            );
            this.addNewMessageProcessor(MessageTemplate.and(
                    MatchPerformative(ACLMessage.REJECT_PROPOSAL),
                    MatchProtocol(Protocol.BOOKING_CHANGE)),
                    "changeRejected"
            );
            this.addNewMessageProcessor(MatchPerformative(ACLMessage.PROPOSE),
                    "handleBookingChangeProposal"
            );
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            processQueryRefBlocks(new Any());
            processBlocksSubscription(new Any());
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void initialize(){
        ClassroomDataTableViewBuilder cdBuilder = new ClassroomDataTableViewBuilder(this);
        MyBookingsTableViewBuilder mbBuilder = new MyBookingsTableViewBuilder(this);
        cdBuilder.build();
        mbBuilder.build();

        bookingHourChoiceBox.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number number2) {
                refreshClassroomDataTableViewIfDataSelected();
            }
        });

        bookingDurationChoiceBox.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number number2) {
                updateHoursLabel();
                refreshClassroomDataTableViewIfDataSelected();
            }

            private void updateHoursLabel() {
                String label = getSelectedDuration() == null || Integer.valueOf(getSelectedDuration()) != 1 ? " godziny" : " godzina";
                hoursLabel.setText(label);
            }
        });
    }

    public void runGui() {
        GUIAgentFactory.controller = this;
        GUIAgentFactory.name = this.getName();
        Application.launch(GUIAgentFactory.class);
    }

    public void makeBookingChangeProposal(String roomName){
        FilterDataField f1 = new FilterDataField("RoomID", roomName);
        FilterBookingDate f2 = new FilterBookingDate(getSelectedDate(), getSelectedHour(), getSelectedDuration());
        try {
            List<Block> blockList = this.bcHandler.getBlocks(f1.and(f2));
            String aidString = blockList.get(0).getData().get("SenderAID");
            AID aid = new AID();
            aid.setName(aidString);
            Integer numberOfMyBookings = findOutNumberOfMyBookings();
            System.out.println("NR BOOK " + numberOfMyBookings);
            Integer priorityOfMyPermissions = findOutPriorityOfMyPermissions(roomName);
            String bookingDate = getSelectedDate().toString();
            String bookingHour = getSelectedHour();
            String bookingDuration = getSelectedDuration();
            Argument arg = new Argument(roomName, bookingDate, bookingHour, bookingDuration,
                    numberOfMyBookings, priorityOfMyPermissions, null);
            sendBookingChangeProposal(aid, arg);
        } catch (IOException | InterruptedException | ClassNotFoundException | NullPointerException e) {
            processException(e);
        }
    }

    private Integer findOutPriorityOfMyPermissions(String room) throws IOException, InterruptedException, ClassNotFoundException {
        Filter f = new FilterDataField("CommunicateName", CommunicateName.ASSIGN_OWNER).
                and(new FilterDataField("OwnerAID", this.getName())).
                and(new FilterDataField("RoomID", room));
        List<Block> blockList = this.bcHandler.getBlocks(f);
        if (blockList != null && !blockList.isEmpty()) {
            System.out.println("PriorityValue.OWNER;");
            return PriorityValue.OWNER;
        }
        System.out.println("PriorityValue.NONE;");
        return PriorityValue.NONE;
    }

    private Integer findOutNumberOfMyBookings() {
        List<Block> blockList = this.bcHandler.getBlocks(new FilterUserBooking(this.getName()));
        return blockList.size();
    }

    void sendBookingChangeProposal(AID agent, Argument argument) throws IOException, InterruptedException {
        ACLMessage propose = new ConversationMessage(ACLMessage.PROPOSE);
        propose.setContent(argument.serialize());
        propose.setProtocol(Protocol.BOOKING_CHANGE);
        propose.addReceiver(agent);
        this.send(propose);
    }

    public void handleBookingChangeProposal(ACLMessage msg) {
        Argument theirsArgument = null;
        try {
            theirsArgument = Argument.deserialize(msg.getContent());
            Integer numberOfMyBookings = findOutNumberOfMyBookings();
            System.out.println("NR BOOK " + numberOfMyBookings);
            Integer priorityOfMyPermissions = findOutPriorityOfMyPermissions(theirsArgument.getRoom());
            Argument myArgument = theirsArgument.makeUpRiposteForThis(numberOfMyBookings, priorityOfMyPermissions, null);
            ACLMessage answer = null;
            System.out.println("ELO" + myArgument.getWeight() + " - - - " + theirsArgument.getWeight());
            if(myArgument.getWeight().compareTo(theirsArgument.getWeight()) > 0){
                answer = new ConversationMessage(ACLMessage.REJECT_PROPOSAL);
            }
            else{
                answer = new ConversationMessage(ACLMessage.ACCEPT_PROPOSAL);
                    cancelBooking(this.getName(),
                            theirsArgument.getRoom(),
                            parseDate(theirsArgument.getBookingDate()),
                            theirsArgument.getBookingHour());
                    bookRoom(msg.getSender().getName(),
                            theirsArgument.getRoom(),
                            parseDate(theirsArgument.getBookingDate()),
                            theirsArgument.getBookingHour(),
                            Integer.valueOf(theirsArgument.getBookingDuration()));
            }
            answer.setProtocol(Protocol.BOOKING_CHANGE);
            answer.addReceiver(msg.getSender());
            this.send(answer);
        } catch (IOException | ClassNotFoundException | InterruptedException e) {
            processException(e);
            sendFailureResponse(msg, "Failure - Argument class object expected as a content");
        }
    }

    public void changeAccepted(ACLMessage msg){
        //runLater jest potrzebne, zeby JavaFX nie rzucal wyjatkow (tylko watek JavaFX moze edytowac GUI)
        Platform.runLater(new Runnable() {
            @Override public void run() {
                logInfo("Agent zgodził się odstąpić salę.");
            }
        });
    }

    public void changeRejected(ACLMessage msg){
        //runLater jest potrzebne, zeby JavaFX nie rzucal wyjatkow (tylko watek JavaFX moze edytowac GUI)
        Platform.runLater(new Runnable() {
            @Override public void run() {
                logInfo("Propozycja nie została przyjęta.");
            }
        });
    }

    public void resignMyBooking(MyBookingsData data) {
        try {
            LocalDate date = parseDate(data.getDate());
            String room = data.getRoom();
            String hour = data.getHour();
            cancelBooking(this.getName(), room, date, hour);
            refreshMyBookingsTableView();
        } catch (InterruptedException | IOException e) {
            processException(e);
        }
        logInfo("Informacja o rezygnacji została dodana do blockchain");
    }

    private void bookRoom(String agent, String roomId, LocalDate date, String hour, Integer duration)
            throws IOException, InterruptedException {
        Platform.runLater(new Runnable() {
            @Override public void run() {
                try {
                    processBlockAddition(new BookClassroom(agent, roomId, date, hour, duration));
                } catch (IOException | InterruptedException e) {
                    processException(e);
                }
            }
        });
    }

    private void cancelBooking(String agent, String roomId, LocalDate date, String hour)
            throws IOException, InterruptedException {
        Platform.runLater(new Runnable() {
            @Override public void run() {
                try {
                    CancelBooking block = new CancelBooking(agent, roomId, date, hour, 1);
                    processBlockAddition(block);
                } catch (IOException | InterruptedException e) {
                    processException(e);
                }
            }
        });
    }

    @FXML
    public void bookButtonClicked() {
        if(!isSelectedRoomAvailable()){
            logInfo("Ta sala jest już zarezerwowana.");
            return;
        }
        String classroomId = getSelectedRoomId();
        LocalDate date = getSelectedDate();
        String hour = getSelectedHour();
        Integer duration = Integer.valueOf(getSelectedDuration());
        if(!WsdUtils.isAnyNullOrEmpty(classroomId, date, hour)){
            try {
                bookRoom(this.getName(), classroomId, date, hour, duration);
                refreshClassroomDataTableViewIfDataSelected();
            } catch (InterruptedException | IOException e) {
                processException(e);
            }
            logInfo("Pomyślnie dodano blok w blockchain.");
        }
        else{
            logInfo("Rezerwacja nie wykonana - nie wybrano daty bądź sali.");
        }
    }

    @FXML
    public void refreshClassroomDataTableViewIfDataSelected() {
        String hour = getSelectedHour();
        LocalDate date = getSelectedDate();
        String duration = getSelectedDuration();
        if(WsdUtils.isAnyNullOrEmpty(date, hour, duration)){
            return;
        }
        try {
            refreshClassroomDataTableView(date, hour, duration);
        } catch (IOException | ClassNotFoundException e) {
            processException(e);
        }
    }

    private void refreshClassroomDataTableView(LocalDate date, String hour, String duration) throws IOException, ClassNotFoundException {
        List<Block> blockList = this.bcHandler.getBlocks((new FilterCreateBlock().
                or(new FilterBookingDate(date, hour, duration))));
        ObservableList<ClassroomData> items = convertBlocksToClassroomData(blockList);
        classroomDataTableView.setItems(items);
    }

    @FXML
    void refreshMyBookingsTableView(){
        List<Block> blockList = this.bcHandler.getBlocks(new FilterUserBooking(this.getName()));
        ObservableList<MyBookingsData> items = convertBlocksToMyBookingsData(blockList);
        myBookingsTableView.setItems(items);
    }

    private ObservableList<ClassroomData> convertBlocksToClassroomData(List<Block> blockList) {
        ObservableList<ClassroomData> result = FXCollections.observableArrayList();
        ObservableList<BookClassroom> bookingInfo = FXCollections.observableArrayList();
        for(Block b : blockList){
            if(b.getData().containsKey("CommunicateName") &&
                    b.getData().get("CommunicateName").equals("CreateClassroom")){
                String name = b.getData().get("RoomID");
                String capacity = b.getData().get("Capacity");
                String tags = b.getData().get("Tags");
                result.add(new ClassroomData(name, tags, capacity, true));
            }
            else if(b instanceof BookClassroom){
                bookingInfo.add((BookClassroom) b);
            }
            else if(b instanceof CancelBooking){
                Predicate<BookClassroom> pred = mbd ->
                        WsdUtils.valueExistsAndEquals(b.getData(), "RoomID", mbd.getData().get("RoomID")) &&
                        WsdUtils.valueExistsAndEquals(b.getData(), "Date", mbd.getData().get("Date")) &&
                        WsdUtils.valueExistsAndEquals(b.getData(), "Hour", mbd.getData().get("Hour"));
                bookingInfo.removeIf(pred);
            }
        }
        for(BookClassroom book : bookingInfo){
            String roomID = book.getData().get("RoomID");
            FilteredList<ClassroomData> list = new FilteredList<>(result, t -> t.getName().equals(roomID));
            if(list.get(0) != null){
                list.get(0).setAvailable(false);
            }
        }
        return result;
    }

    private ObservableList<MyBookingsData> convertBlocksToMyBookingsData(List<Block> blockList) {
        ObservableList<MyBookingsData> result = FXCollections.observableArrayList();
        for(Block b : blockList){
                if(b instanceof BookClassroom){
                    String room = b.getData().get("RoomID");
                    String date = b.getData().get("Date");
                    String hour = b.getData().get("Hour");
                    String duration = b.getData().get("Duration");
                    result.add(new MyBookingsData(room, date, hour, Integer.valueOf(duration)));
                }
                else if(b instanceof CancelBooking){
                    Predicate<MyBookingsData> pred = mbd ->
                            WsdUtils.valueExistsAndEquals(b.getData(), "RoomID", mbd.getRoom()) &&
                            WsdUtils.valueExistsAndEquals(b.getData(), "Date", mbd.getDate()) &&
                            WsdUtils.valueExistsAndEquals(b.getData(), "Hour", mbd.getHour());
                    result.removeIf(pred);
                }
        }
        return result;
    }

    public TableView getClassroomDataTableView(){
        return classroomDataTableView;
    }

    public TableView getMyBookingsTableView(){
        return myBookingsTableView;
    }

    String getSelectedDuration(){
        int durIndex = bookingDurationChoiceBox.getSelectionModel().selectedIndexProperty().getValue();
        String dur = durIndex >= 0 ? bookingDurationChoiceBox.getItems().get(durIndex) : null;
        return dur != null ? dur : null;
    }

    String getSelectedHour(){
        int hourIndex = bookingHourChoiceBox.getSelectionModel().selectedIndexProperty().getValue();
        String hour = hourIndex >= 0 ? bookingHourChoiceBox.getItems().get(hourIndex) : null;
        return hour;
    }

    LocalDate getSelectedDate(){
        return bookingDatePicker.getValue();
    }

    String getSelectedRoomId(){
        ClassroomData selectedClassroomData = getSelectedClassroomData();
        String result = selectedClassroomData != null ? selectedClassroomData.getName() : null;
        return result;
    }

    boolean isSelectedRoomAvailable(){
        ClassroomData selectedClassroomData = getSelectedClassroomData();
        return selectedClassroomData.getAvailable();
    }

    ClassroomData getSelectedClassroomData(){
        Object selected = classroomDataTableView.getSelectionModel().getSelectedItem();
        return (ClassroomData) selected;
    }

    List<Block> extractBlockListFromMessage(ACLMessage a) throws IOException, ClassNotFoundException {
        Object o = Serializer.fromString(a.getContent().toString());
        return (List<Block>) o;
    }

    @Override
    public void logInfo(String message){
        Logger.info(this, message);
        logMessageOnGUI(message);
    }

    void logMessageOnGUI(String s){
        if(messageTextArea != null){
            messageTextArea.clear();
            messageTextArea.setText(s);
        }
    }

    void processException(Exception e){
        e.printStackTrace();
        logMessageOnGUI(e.getMessage());
    }

    void resetMessageOnGUI(){
        messageTextArea.clear();
    }

    LocalDate parseDate(String date){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return LocalDate.parse(date, formatter);
    }
}
