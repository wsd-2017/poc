package view.builders;

import javafx.scene.control.TableView;
import view.GUIAgent;

/**
 * Created by Radek on 2018-01-20.
 */
public abstract class TableViewBuilder {

    GUIAgent agent;

    TableView tableView;

    TableViewBuilder(GUIAgent agent){
        this.agent = agent;
    }

    public void build(){
        setPlaceholder(tableView);
        configureCellValues(tableView);
        configureRows(tableView);
    }

    protected abstract void configureRows(TableView tableView);

    protected abstract void configureCellValues(TableView tableView);

    protected abstract void setPlaceholder(TableView tableView);
}
