package view.builders;

import javafx.beans.binding.Bindings;
import javafx.collections.ObservableList;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;
import view.GUIAgent;
import view.dao.ClassroomData;
import view.dao.MyBookingsData;

import java.lang.reflect.Field;

/**
 * Created by Radek on 2018-01-19.
 */
public class MyBookingsTableViewBuilder extends TableViewBuilder {

    public MyBookingsTableViewBuilder(GUIAgent agent){
        super(agent);
        this.tableView = agent.getMyBookingsTableView();
    }

    @Override
    protected void configureRows(TableView tableView) {
        tableView.setRowFactory(
                new Callback<TableView<MyBookingsData>, TableRow<MyBookingsData>>() {
                    @Override
                    public TableRow<MyBookingsData> call(TableView<MyBookingsData> tableView) {
                        return addContextMenu();
                    }
                });
    }

    @Override
    protected void configureCellValues(TableView tableView) {
        ObservableList<TableColumn> cols = tableView.getColumns();
        Field[] fields = MyBookingsData.class.getDeclaredFields();
        cols.get(0).setCellValueFactory(new PropertyValueFactory<>(fields[0].getName()));
        cols.get(1).setCellValueFactory(new PropertyValueFactory<>(fields[1].getName()));
        cols.get(2).setCellValueFactory(new PropertyValueFactory<>(fields[2].getName()));
        cols.get(3).setCellValueFactory(new PropertyValueFactory<>(fields[3].getName()));
    }

    @Override
    protected void setPlaceholder(TableView tableView) {
        tableView.setPlaceholder(new Label("Nie posiadasz teraz żadnych rezerwacji."));
    }

    private TableRow addContextMenu(){
        final TableRow<MyBookingsData> row = new TableRow<>();
        final ContextMenu rowMenu = new ContextMenu();
        MenuItem resignItem = createResignMenuItem(row);
        rowMenu.getItems().addAll(resignItem);

        // nie wyswietlaj menu dla pustych wierszy w tabeli
        row.contextMenuProperty().bind(
                Bindings.when(Bindings.isNotNull(row.itemProperty()))
                        .then(rowMenu)
                        .otherwise((ContextMenu)null));
        return row;
    }

    private MenuItem createResignMenuItem( TableRow<MyBookingsData> row) {
        MenuItem item = new MenuItem();
        item.textProperty().bind(Bindings.format("Zrezygnuj z rezerwacji."));
        item.setOnAction(event -> {
            MyBookingsData clickedItem = row.getItem();
            agent.resignMyBooking(clickedItem);
        });
        return item;
    }
}
