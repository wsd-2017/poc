package view.builders;

import javafx.beans.binding.Bindings;
import javafx.collections.ObservableList;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;
import view.GUIAgent;
import view.dao.ClassroomData;
import view.dao.MyBookingsData;

import java.lang.reflect.Field;

/**
 * Created by Radek on 2018-01-19.
 */
public class ClassroomDataTableViewBuilder extends TableViewBuilder {

    public ClassroomDataTableViewBuilder(GUIAgent agent){
        super(agent);
        this.tableView = agent.getClassroomDataTableView();
    }

    @Override
    protected void configureRows(TableView tableView) {
        tableView.setRowFactory(
                new Callback<TableView<ClassroomData>, TableRow<ClassroomData>>() {
                    @Override
                    public TableRow<ClassroomData> call(TableView<ClassroomData> tableView) {
                        return addContextMenu();
                    }
                });
    }

    private TableRow addContextMenu(){
        final TableRow<ClassroomData> row = new TableRow<>();
        final ContextMenu rowMenu = new ContextMenu();

        MenuItem requestResignationItem = createRequestResignationMenuItem(row);
        MenuItem bookingItem = createBookMenuItem(row);
        MenuItem refreshItem = createRefreshMenuItem(row);
        rowMenu.getItems().addAll(bookingItem, requestResignationItem, refreshItem);

        // only display context menu for non-null items:
        row.contextMenuProperty().bind(
                Bindings.when(Bindings.isNotNull(row.itemProperty()))
                        .then(rowMenu)
                        .otherwise((ContextMenu)null));
        return row;
    }

    private MenuItem createRefreshMenuItem(TableRow<ClassroomData> row) {
        MenuItem item = new MenuItem();
        item.textProperty().bind(Bindings.format("Odśwież listę sal"));
        item.setOnAction(event -> {
            agent.refreshClassroomDataTableViewIfDataSelected();
        });
        return item;
    }

    private MenuItem createBookMenuItem(TableRow<ClassroomData> row) {
        MenuItem item = new MenuItem();
        String itemName = row.itemProperty().getValue() != null ? row.itemProperty().getValue().getName() : "";
        item.textProperty().bind(Bindings.format("Zarezerwuj %s", ""  ));
        item.setOnAction(event -> {
            agent.bookButtonClicked();
        });
        return item;
    }

    private MenuItem createRequestResignationMenuItem(TableRow<ClassroomData> row) {
        MenuItem item = new MenuItem();
        item.textProperty().bind(Bindings.format("Poproś o zamianę"));
        item.setOnAction(event -> {
            ClassroomData clickedItem = row.getItem();
            agent.makeBookingChangeProposal(clickedItem.getName());
        });
        return item;
    }

    @Override
    protected void configureCellValues(TableView tableView) {
        ObservableList<TableColumn> cols = tableView.getColumns();
        Field[] fields = ClassroomData.class.getDeclaredFields();
        cols.get(0).setCellValueFactory(new PropertyValueFactory<>(fields[0].getName()));
        cols.get(1).setCellValueFactory(new PropertyValueFactory<>(fields[1].getName()));
        cols.get(2).setCellValueFactory(new PropertyValueFactory<>(fields[2].getName()));
        cols.get(3).setCellValueFactory(new PropertyValueFactory<>(fields[3].getName()));
    }

    @Override
    protected void setPlaceholder(TableView tableView) {
        tableView.setPlaceholder(new Label("Wypełnij pola powyżej, aby wyświetlić listę dostępnych sal."));
    }
}
