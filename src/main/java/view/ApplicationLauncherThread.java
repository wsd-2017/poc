package view;

import javafx.application.Application;

public class ApplicationLauncherThread extends Thread{
    private IGUIRunnable iguiRunnable;
    public ApplicationLauncherThread(IGUIRunnable iguiRunnable) {
        this.iguiRunnable = iguiRunnable;
    }

    @Override
    public void run() {
        iguiRunnable.runGui();
    }
}
