package view;

/**
 * Created by Radek on 2017-12-16.
 */

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class GUIAgentFactory extends Application {

    static public Object controller;
    static public String name;

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/gui.fxml"));
        loader.setController(controller);
        final Parent root = loader.load();
        final Scene scene = new Scene(root);
        name = name != null ? name : "GUIAgent";
        primaryStage.setTitle(name);
        primaryStage.setResizable(false);
        primaryStage.setScene(scene);
        //primaryStage.sizeToScene();
        primaryStage.centerOnScreen();
        primaryStage.show();
    }
}