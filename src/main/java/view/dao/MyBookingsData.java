package view.dao;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * Created by Radek on 2018-01-15.
 */
public class MyBookingsData
{
    private final SimpleStringProperty room;
    private final SimpleStringProperty date;
    private final SimpleStringProperty hour;
    private final SimpleIntegerProperty duration;

    public MyBookingsData(String room, String date, String hour, Integer duration) {
        this.room = new SimpleStringProperty(room);
        this.date = new SimpleStringProperty(date);
        this.hour = new SimpleStringProperty(hour);
        this.duration = new SimpleIntegerProperty(duration);
    }

    public String getRoom() {
        return room.get();
    }
    public void setRoom(String room) {
        this.room.set(room);
    }

    public String getDate() {
        return date.get();
    }
    public void setDate(String date) {
        this.date.set(date);
    }

    public String getHour() {
        return hour.get();
    }
    public void setHour(String hour) {
        this.hour.set(hour);
    }

    public Integer getDuration() {
        return duration.get();
    }
    public void setDuration(Integer duration) {
        this.duration.set(duration);
    }
}
