package view.dao;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * Created by Radek on 2018-01-15.
 */
public class ClassroomData
{
    private final SimpleStringProperty name;
    private final SimpleStringProperty type;
    private final SimpleStringProperty capacity;
    private final SimpleBooleanProperty available;

    public ClassroomData(String name, String type, String capacity, Boolean available) {
        this.name = new SimpleStringProperty(name);
        this.type = new SimpleStringProperty(type);
        this.capacity = new SimpleStringProperty(capacity);
        this.available = new SimpleBooleanProperty(available);
    }

    public String getName() {
        return name.get();
    }
    public void setName(String name) {
        this.name.set(name);
    }

    public String getType() {
        return type.get();
    }
    public void setType(String type) {
        this.type.set(type);
    }

    public String getCapacity() {
        return capacity.get();
    }
    public void setCapacity(String capacity) {
        this.capacity.set(capacity);
    }

    public Boolean getAvailable() {
        return available.get();
    }
    public void setAvailable(Boolean available) {
        this.available.set(available);
    }
}
