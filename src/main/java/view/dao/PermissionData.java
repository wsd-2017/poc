package view.dao;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * Created by Radek on 2018-01-15.
 */
public class PermissionData
{
    private final SimpleStringProperty classroom;
    private final SimpleStringProperty agent;

    public PermissionData(String classroom, String agent) {
        this.classroom = new SimpleStringProperty(classroom);
        this.agent = new SimpleStringProperty(agent);
    }

    public String getClassroom() {
        return classroom.get();
    }
    public void setClassroom(String classroom) {
        this.classroom.set(classroom);
    }

    public String getAgent() {
        return agent.get();
    }
    public void setAgent(String agent) {
        this.agent.set(agent);
    }
}
