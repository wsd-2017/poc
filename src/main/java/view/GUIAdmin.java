package view;
/**
 * Created by Radek on 2017-12-16.
 */

import agents.Administrator;
import agents.utils.Permission;
import blockchain.Block;
import blockchain.Serializer;
import blockchain.filters.*;
import jade.core.AID;
import jade.lang.acl.ACLMessage;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import misc.Logger;
import misc.WsdUtils;
import view.dao.ClassroomData;
import jade.domain.AMSService;
import jade.domain.FIPAAgentManagement.*;
import view.dao.PermissionData;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import static java.lang.Thread.sleep;

public class GUIAdmin extends Administrator implements IGUIRunnable  {

    @FXML
    TableView classroomDataTableView;

    @FXML
    TableView permissionDataTableView;

    @FXML
    TextField classroomNameTextField;

    @FXML
    TextField classroomCapacityTextField;

    @FXML
    TextField classroomTagsTextField;

    @FXML
    TextField newPermissionClassroomTextField;

    @FXML
    ChoiceBox<AID> newPermissionAgentChoiceBox;

    @FXML
    ChoiceBox<String> permissionListChoiceBox;

    @FXML
    ChoiceBox permissionAddChoiceBox;

    @FXML
    TextArea messageTextArea;

    public GUIAdmin() throws NoSuchAlgorithmException, NoSuchMethodException {
        super();
    }

    @Override
    protected void setup() {
        super.setup();
        new ApplicationLauncherThread(this).start();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            processQueryRefBlocks(new Any());
            processBlocksSubscription(new Any());
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void initialize() {
        makeTextFieldNumberOnly(classroomCapacityTextField);
        ObservableList<TableColumn> cols = classroomDataTableView.getColumns();
        cols.get(0).setCellValueFactory(new PropertyValueFactory<>("name"));
        cols.get(1).setCellValueFactory(new PropertyValueFactory<>("capacity"));
        cols.get(2).setCellValueFactory(new PropertyValueFactory<>("type"));

        cols = permissionDataTableView.getColumns();
        cols.get(0).setCellValueFactory(new PropertyValueFactory<>("classroom"));
        cols.get(1).setCellValueFactory(new PropertyValueFactory<>("agent"));
    }

    public void runGui() {
        GUIAdminFactory.controller = this;
        Application.launch(GUIAdminFactory.class);
    }

    @FXML
    void refreshClassroomList() throws IOException, InterruptedException{
        List<Block> blockList = this.bcHandler.getBlocks(new FilterCreateBlock());
        classroomDataTableView.setItems(prepareClassroomItems(blockList));
    }

    private ObservableList<ClassroomData> prepareClassroomItems(List<Block> blockList) {
        ObservableList<ClassroomData> result = FXCollections.observableArrayList();
        for(Block b : blockList){
            String name = b.getData().get("RoomID");
            String capacity = b.getData().get("Capacity");
            String tags = b.getData().get("Tags");
            result.add(new ClassroomData(name, tags, capacity, false));
        }
        return result;
    }

    @FXML
    void refreshPermissionList(){
        logInfo("Odpytywanie Blockchain o listę właścicieli...");
        try {
            String permissionType = permissionListChoiceBox.getSelectionModel().getSelectedItem();

            switch (permissionType != null ? permissionType : ""){
                case "Właściciel":
                    fillPermissionDataTableView(new FilterDataField("CommunicateName", "AssignOwner"), "OwnerAID");
                    break;
                case "Użytkownik stały":
                    fillPermissionDataTableView(new FilterDataField("CommunicateName", "AssignPermission"), "ClassUserAID");
                    break;
                case "Sterownik zamka":
                    fillPermissionDataTableView(new FilterDataField("CommunicateName", "AssignGatekeeper"), "GatekeeperAID");
                    break;
                default:
                    logInfo("Wybierz rodzaj uprawnień do wyświetlania.");
            }
        } catch (InterruptedException | IOException | ClassNotFoundException e) {
            processException(e);
        }
    }

    private void fillPermissionDataTableView(AbstractFilter f, String aidField) throws IOException, InterruptedException, ClassNotFoundException {
        List<Block> blockList = this.bcHandler.getBlocks(f);
        permissionDataTableView.setItems(preparePermissionItems(blockList, aidField));
        logInfo("Otrzymano odpowiedź z Blockchain. ");
    }

    ObservableList<PermissionData> preparePermissionItems(List<Block> blockList, String aidField) {
        ObservableList<PermissionData> result = FXCollections.observableArrayList();
        for(Block b : blockList){
            String name = b.getData().get("RoomID");
            String agent = b.getData().get(aidField);
            result.add(new PermissionData(name, agent));
        }
        return result;
    }

    @FXML
    void addClassroomButtonClicked(){
        logInfo("Rozpoczęto dodawanie sali do Blockchain...");
        String name = classroomNameTextField.getText();
        String capacity = classroomCapacityTextField.getText();
        String tags = classroomTagsTextField.getText();

        if(!WsdUtils.isAnyNullOrEmpty(name, capacity, tags)){
            try {
                addClassRoom(name, Integer.valueOf(capacity), tags);
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
                logMessageOnGUI(e.getMessage());
            }
        }
        else{
            logMessageOnGUI("Nie wszystkie pola zostały wypełnione.");
        }
    }

    @FXML
    void addPermissionButtonClicked(){
        logInfo("Rozpoczęto dodawanie nowego uprawnienia do Blockchain...");
        Object s = permissionAddChoiceBox.getSelectionModel().getSelectedItem();
        String permission = (String) s;
        String classroom = newPermissionClassroomTextField.getText();
        AID agent = newPermissionAgentChoiceBox.getSelectionModel().getSelectedItem();
        if(!WsdUtils.isAnyNullOrEmpty(permission, classroom, agent)){
            try {
                switch (permission){
                    case "Właściciel":
                        logInfo("Dodawanie nowego właściciela...");
                        addAssignOwner(classroom, agent);
                        break;
                    case "Użytkownik stały":
                        logInfo("Dodawanie nowego użytkownika stałego...");
                        addAssignPermission(classroom, agent, Permission.PERMAMENT, 1);//todo priority
                        break;
                    case "Sterownik zamka":
                        logInfo("Dodawanie nowego sterownika zamka...");
                        addAssignGatekeeper(classroom, agent);
                        break;
                    default:
                        logInfo("Error: Zły typ uprawnienia. Skontaktuj się z developerem.");
                }
            } catch (IOException | InterruptedException e) {
                processException(e);
            }
        }
        else{
            logMessageOnGUI("Nie wszystkie pola zostały wypełnione.");
        }
    }

    @FXML
    void setAvailableAgents(){
        ObservableList<AID> list = FXCollections.observableArrayList();
        AMSAgentDescription [] agents = null;
        try {
            SearchConstraints c = new SearchConstraints();
            c.setMaxResults ( new Long(-1) );
            agents = AMSService.search( this, new AMSAgentDescription (), c );
            for(AMSAgentDescription desc : agents){
                AID agentID = desc.getName();
                list.add(agentID);
            }
        }
        catch (Exception e) {
            processException(e);
        }
        newPermissionAgentChoiceBox.setItems(list);
    }

    void processException(Exception e){
        e.printStackTrace();
        logMessageOnGUI(e.getMessage());
    }

    @Override
    public void logInfo(String message){
        Logger.info(this, message);
        logMessageOnGUI(message);
    }

    void logMessageOnGUI(String s){
        if(messageTextArea != null){
            messageTextArea.clear();
            messageTextArea.setText(s);
        }
    }

    @FXML
    void resetMessageOnGUI(){
        messageTextArea.clear();
    }

    void makeTextFieldNumberOnly(TextField classroomCapacityTextField) {
        classroomCapacityTextField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue,
                                String newValue) {
                if (!newValue.matches("\\d*")) {
                    classroomCapacityTextField.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });
    }
}
