package agents;

import agents.utils.MessageListenerThread;
import agents.utils.MessageProcessor;
import jade.core.AID;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import misc.Logger;
import org.apache.log4j.BasicConfigurator;

import java.util.*;

import static java.lang.Thread.sleep;

public class ClassroomReservationSystemAgent extends Agent {

    public Vector<ACLMessage> messagesWaitingForResponse;
    public Vector<ACLMessage> responsesAwaited;
    public Vector<MessageProcessor> messageProcessors;
    private Random randomGenerator = new Random();
    static boolean configured = false;
    private MessageListenerThread messageListenerThread;
    public ClassroomReservationSystemAgent() throws NoSuchMethodException {
        super();
        messagesWaitingForResponse = new Vector<ACLMessage>();
        responsesAwaited = new Vector<ACLMessage>();
        messageProcessors = new Vector<MessageProcessor>();
        if(!configured) {
            BasicConfigurator.configure();
            configured = true;
        }
        messageListenerThread = new MessageListenerThread(this, messageProcessors);
        messageListenerThread.start();
    }

    @Override
    protected void setup() {
        super.setup();
        try {
            addNewMessageProcessor(
                    MessageTemplate.MatchAll(),
                    "hamdleResponseIfExpected"
            );
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }


    public void register(String serviceType, String serviceName) {
        this.logInfo(String.format("Registering as service %s / %s.", serviceType, serviceName));
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setType(serviceType);
        sd.setName(serviceName);

        dfd.addServices(sd);
        try {
            DFService.register(this, dfd);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }
    }

    public List<AID> findAgentsOfService(String serviceType, String serviceName) {
        DFAgentDescription template = new DFAgentDescription();
        ServiceDescription sd = new ServiceDescription();
        sd.setType(serviceType);
        sd.setName(serviceName);
        template.addServices(sd);
        List<AID> agents = new ArrayList<>();
        try {
            DFAgentDescription[] results = DFService.search(this, template);
            for (DFAgentDescription result : results) {
                agents.add(result.getName());
            }
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }
        return agents;
    }

    public AID findRandomAgentOfService(String serviceType, String serviceName) throws InterruptedException {
        return findRandomAgentOfService(serviceType, serviceName, true, 1000);
    }

    public AID findRandomAgentOfService(String serviceType, String serviceName, boolean waitForAgents, int sleepingStep) throws InterruptedException {
        List<AID> agents = findAgentsOfService(serviceType, serviceName);
        if(!waitForAgents && agents.size() < 1){
            throw new RuntimeException("No agents of specified type/name.");
        }
        while (agents.size() < 1){
            agents = findAgentsOfService(serviceType, serviceName);
            this.logInfo(String.format("Waiting for agent of service %s/%s", serviceType, serviceName));
            sleep(sleepingStep);
        }
        int index = randomGenerator.nextInt(agents.size());
        return agents.get(index);
    }

    public void logInfo(String nessage){
        Logger.info(this, nessage);
    }

    public void logError(String nessage){
        Logger.info(this, nessage);
    }

    public ACLMessage waitForResponse(ACLMessage message) throws InterruptedException {
        messagesWaitingForResponse.add(message);
        this.logInfo("Waiting for response.");
        while (true){
            ACLMessage response = fetchResponse(message);
            if(response == null){
                sleep(10);
            } else {
                this.logInfo("Response got.");
                return response;
            }
        }
    }

    private ACLMessage fetchResponse(ACLMessage message){
        ACLMessage result = null;
        for (ACLMessage response:responsesAwaited) {
            if(response.getConversationId().equals(message.getConversationId())){
                result = response;
                break;
            }
        }
        responsesAwaited.remove(result);
        return result;
        //throw new RuntimeException("Response not found.");
    }

    public void addMessageProcessor(MessageProcessor processor){
        if(!messageProcessors.contains(processor)) messageProcessors.add(processor);
    }

    public void removeMessageProcessor(MessageProcessor processor){
        messageProcessors.remove(processor);
    }

    protected void addNewMessageProcessor(MessageTemplate template, String methodName) throws NoSuchMethodException {
        MessageProcessor processor = new MessageProcessor(
                template,
                this.getClass().getMethod(methodName, ACLMessage.class),
                this
        );
        this.addMessageProcessor(processor);
    }

    public void sendFailureResponse(ACLMessage request, String message){
        this.sendResponse(request, ACLMessage.FAILURE, message);
    }

    public void sendConfirmResponse(ACLMessage request, String message){
        this.sendResponse(request, ACLMessage.CONFIRM, message);
    }

    public void sendResponse(ACLMessage request, int perf, String message){
        ACLMessage reply = request.createReply();
        reply.setPerformative(perf);
        reply.setContent(message);
        this.send(reply);
    }

    public void hamdleResponseIfExpected(ACLMessage message){
        Vector<ACLMessage> messagesToRemove = new Vector<ACLMessage>();
        for (ACLMessage messageWaiting :messagesWaitingForResponse ) {
            if (Objects.equals(message.getConversationId(), message.getConversationId())) {
                messagesToRemove.add(messageWaiting);
                responsesAwaited.add(message);
            }
        }
        //messagesWaitingForResponse.removeAll(messagesToRemove);
    }

    @Override
    protected void finalize() throws Throwable {
        messageListenerThread.stopRunning();
        super.finalize();
    }
}
