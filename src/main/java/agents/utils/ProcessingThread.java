package agents.utils;

import jade.lang.acl.ACLMessage;

import java.lang.reflect.InvocationTargetException;

public class ProcessingThread extends Thread {

    private MessageProcessor processor;
    ACLMessage message;

    public ProcessingThread(MessageProcessor processor, ACLMessage message) {
        super();
        this.processor = processor;
        this.message = message;
    }

    @Override
    public void run() {
        try {
            processor.process(message);
        } catch (InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
