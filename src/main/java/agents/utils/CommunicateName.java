package agents.utils;

/**
 * Created by Radek on 2018-01-20.
 */
public class CommunicateName {
    public static final String CANCEL_BOOKING = "CancelBooking";
    public static final String BOOK_CLASSROOM = "BookClassroom";
    public static final String ASSIGN_OWNER = "AssignOwner";

}
