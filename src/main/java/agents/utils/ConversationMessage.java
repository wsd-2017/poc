package agents.utils;

import jade.lang.acl.ACLMessage;

import java.util.Random;
import java.util.UUID;

public class ConversationMessage extends ACLMessage {
    public ConversationMessage(int perf) {
        super(perf);
        this.setConversationId(UUID.randomUUID().toString());
    }
}
