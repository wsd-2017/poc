package agents.utils;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import misc.Logger;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class MessageListenerThread extends Thread{

    protected List<MessageProcessor> processors;
    protected Agent myAgent;
    protected AtomicBoolean shouldStop;

    public MessageListenerThread(Agent agent, List<MessageProcessor> processors) {
        this.processors = processors;
        myAgent = agent;
        shouldStop = new AtomicBoolean();
        shouldStop.set(false);
    }

    @Override
    public void run() {
        while( !shouldStop.get()) {
            try {
                sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            ACLMessage message = myAgent.receive();
            if (message != null) {
                for (MessageProcessor processor : processors) {
                    new ProcessingThread(processor, message).start();
                }
            }
        }
    }


    public void stopRunning(){
        shouldStop.set(true);
    }

}
