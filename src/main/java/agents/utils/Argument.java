package agents.utils;

import blockchain.Block;
import blockchain.Serializer;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

public class Argument implements Serializable {

    String roomINeed;
    String bookingDate;
    String bookingHour;
    String bookingDuration;
    Integer numberOfMyBookings;
    Integer priorityOfMyPermission;
    List<String> necessaryTags;

    public Argument(String roomINeed, String bookingDate, String bookingHour, String bookingDuration,
                    Integer numberOfMyBookings, Integer priorityOfMyPermission, List<String> necessaryTags){
        this.roomINeed = roomINeed;
        this.bookingDate = bookingDate;
        this.bookingHour = bookingHour;
        this.bookingDuration = bookingDuration;
        this.numberOfMyBookings = numberOfMyBookings;
        this.priorityOfMyPermission = priorityOfMyPermission;
        this.necessaryTags = necessaryTags;
    }

    public String getRoom(){
        return roomINeed;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public String getBookingHour() {
        return bookingHour;
    }

    public String getBookingDuration() {
        return bookingDuration;
    }

    public Integer getWeight(){
        //sample formula for measuring strength of argument
        return priorityOfMyPermission - numberOfMyBookings*2;
    }

    public Argument makeUpRiposteForThis(Integer numberOfMyBookings,
                                         Integer priorityOfMyPermission,
                                         List<String> necessaryTags){
        Argument riposte = new Argument(roomINeed, bookingDate, bookingHour, bookingDuration,
                numberOfMyBookings, priorityOfMyPermission, necessaryTags);
        return riposte;
    }

    public String serialize() throws IOException {
        return Serializer.toString( this);
    }

    public static Argument deserialize(String s) throws IOException, ClassNotFoundException {
        return (Argument)Serializer.fromString(s);
    }

}
