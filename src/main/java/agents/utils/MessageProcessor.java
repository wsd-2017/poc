package agents.utils;

import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

public class MessageProcessor {

    private final Object invokingObject;
    private MessageTemplate template;
    private Method method;
    public MessageProcessor(MessageTemplate template, Method method, Object invokingObject) {
        this.template = template;
        this.method = method;
        this.invokingObject = invokingObject;
    }

    public void process(ACLMessage message) throws InvocationTargetException, IllegalAccessException {
        if(template.match(message)){
            try {
                this.method.invoke(invokingObject, message);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println(Arrays.toString(e.getStackTrace()));
            }
        }
    }
}


