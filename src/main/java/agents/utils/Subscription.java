package agents.utils;

import blockchain.Serializer;
import blockchain.filters.Filter;
import jade.core.AID;
import jade.lang.acl.ACLMessage;

import java.io.IOException;

public class Subscription {
    private AID subscriber;
    private Filter filter;
    private ACLMessage request;

    public Subscription(ACLMessage request) throws IOException, ClassNotFoundException {
        this.filter = (Filter) Serializer.fromString(request.getContent());
        this.subscriber = request.getSender();
        this.request = request;
    }

    public AID getSubscriber() {
        return subscriber;
    }

    public void setSubscriber(AID subscriber) {
        this.subscriber = subscriber;
    }

    public Filter getFilter() {
        return filter;
    }

    public void setFilter(Filter filter) {
        this.filter = filter;
    }

    public ACLMessage getRequest() {
        return request;
    }

    public void setRequest(ACLMessage request) {
        this.request = request;
    }

}
