package agents;

import agents.utils.Permission;
import blockchain.blocks.AssignGatekeeper;
import blockchain.blocks.AssignOwner;
import blockchain.blocks.AssignPermission;
import blockchain.blocks.CreateClassroom;
import jade.core.AID;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

public class Administrator extends User {
    public Administrator() throws NoSuchAlgorithmException, NoSuchMethodException {
        super();
    }

    @Override
    protected void setup() {
        super.setup();
    }

    public void addClassRoom(String classroomId, int capacity, String tags) throws IOException, InterruptedException {
        CreateClassroom block = new CreateClassroom(this.getName(), classroomId, capacity, tags);
        processBlockAddition(block);
    }

    public void addAssignGatekeeper(String roomId, AID gatekeeper) throws IOException, InterruptedException {
        AssignGatekeeper assignGatekeeper = new AssignGatekeeper(this.getName(), roomId, gatekeeper.getName());
        processBlockAddition(assignGatekeeper);
    }

    public void addAssignPermission(String roomId, AID user, Permission permission, Integer priority) throws IOException, InterruptedException {
        AssignPermission assignPermission = new AssignPermission(this.getName(), roomId, user.getName(), permission, priority);
        processBlockAddition(assignPermission);
    }

    public void addAssignOwner(String roomId, AID user) throws IOException, InterruptedException {
        AssignOwner assignOwner = new AssignOwner(this.getName(), roomId, user.getName());
        processBlockAddition(assignOwner);
    }
}
