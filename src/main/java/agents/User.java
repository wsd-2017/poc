package agents;

import agents.utils.ConversationMessage;
import blockchain.Block;
import blockchain.filters.FilterDataField;
import jade.core.AID;
import jade.lang.acl.ACLMessage;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

public class User extends BlockchainClient {
    public User() throws NoSuchAlgorithmException, NoSuchMethodException {
        super();
    }

    @Override
    protected void setup() {
        super.setup();
    }

    public ACLMessage requestDoorsAction(String classroomId, String protocol) throws IOException, ClassNotFoundException {
        ACLMessage request = new ConversationMessage(ACLMessage.REQUEST);
        request.setContent(classroomId);
        AID gatekeeper = findGatekeeperOfClassroom(classroomId);
        request.addReceiver(gatekeeper);
        request.setProtocol(protocol);
        this.send(request);
        return request;
    }

    public ACLMessage requestOpenDoors(String classroomId) throws IOException, ClassNotFoundException {
        return requestDoorsAction(classroomId, "OpenDoors");
    }

    public ACLMessage requestCloseDoors(String classroomId) throws IOException, ClassNotFoundException {
        return requestDoorsAction(classroomId, "CloseDoors");
    }

    private AID findGatekeeperOfClassroom(String classroomId) throws IOException, ClassNotFoundException {
        List<Block> blocks = this.bcHandler.getBlocks(
                new FilterDataField("CommunicateName", "AssignGatekeeper")
                        .and(new FilterDataField("RoomID", classroomId))
        );
        Block lastBlock = blocks.get(blocks.size() - 1);
        String gatekeeperName = lastBlock.getData().get("GatekeeperAID");
        List<AID> gatekeepers = findAgentsOfService("SwitchDoorsService", "");
        for (AID aid:gatekeepers
             ) {
            if(aid.getName().equals(gatekeeperName)){
                return aid;
            }
        }
        return null;
    }

}
