package agents;

import agents.utils.LockState;
import blockchain.filters.FilterGatekeeper;
import jade.core.AID;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

public class Gatekeeper extends BlockchainClient {

    private LockState lockState;
    private String classroomID;

    public Gatekeeper() throws NoSuchAlgorithmException, NoSuchMethodException {
        super();
    }

    @Override
    protected void setup() {
        super.setup();
        try {
            addNewMessageProcessor(
                    MessageTemplate.and(
                            MessageTemplate.MatchPerformative(ACLMessage.REQUEST),
                            MessageTemplate.MatchProtocol("OpenDoors")
                    ),
                    "handleOpenDoorsRequest"
            );
            addNewMessageProcessor(
                    MessageTemplate.and(
                            MessageTemplate.MatchPerformative(ACLMessage.REQUEST),
                            MessageTemplate.MatchProtocol("CloseDoors")
                    ),
                    "handleCloseDoorsRequest"
            );
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        register("SwitchDoorsService", "");
    }

    public void askForSuitableBlocks() throws IOException, InterruptedException {
        FilterGatekeeper filter = new FilterGatekeeper(this.getName());
        processQueryRefBlocks(filter);
    }

    protected void openDoors(){
        lockState = LockState.OPEN;
    }

    protected void closeDoors(){
        lockState = LockState.CLOSED;
    }

    protected void handleOpenDoorsRequest(ACLMessage request){
        boolean shouldContinue = checkPermission(request.getSender());
        if(shouldContinue){
            openDoors();
        }
    }

    protected void handleCloseDoorsRequest(ACLMessage request){
        boolean shouldContinue = checkPermission(request.getSender());
        if(shouldContinue){
            closeDoors();
        }
    }

    protected boolean checkPermission(AID aid){
        // todo implement: check permission in BC
        return true;
    }


}
