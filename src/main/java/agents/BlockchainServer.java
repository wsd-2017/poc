package agents;

import agents.utils.ConversationMessage;
import agents.utils.Subscription;
import blockchain.Block;
import blockchain.BlockConflictException;
import blockchain.BlockchainHandler;
import blockchain.Serializer;
import blockchain.filters.Filter;
import jade.core.AID;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import misc.Logger;

import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import static jade.lang.acl.MessageTemplate.MatchPerformative;
import static jade.lang.acl.MessageTemplate.MatchProtocol;

public class BlockchainServer extends ClassroomReservationSystemAgent {

    private BlockchainHandler bcHandler;
    private Random randomGenerator;
    private Vector<Subscription> subscriptions;

    public BlockchainServer() throws NoSuchMethodException {
        subscriptions = new Vector<Subscription>();
        randomGenerator = new Random();
        this.addNewMessageProcessor(
                MessageTemplate.and(MatchPerformative(ACLMessage.REQUEST),MatchProtocol("BlockAddition")),
                "handleBlockAdditionRequest"
        );
        this.addNewMessageProcessor(
                MatchPerformative(ACLMessage.QUERY_REF),
                "handleQueryRefBlocks"
        );
        this.addNewMessageProcessor(
                MessageTemplate.and(MatchPerformative(ACLMessage.PROPAGATE), MatchProtocol("PropagateBlock")),
                "handlePropagateBlock"
        );
        this.addNewMessageProcessor(
                MessageTemplate.and(MatchPerformative(ACLMessage.SUBSCRIBE), MatchProtocol("SubscribeBlocks")),
                "handleSubscribeBlocks"
        );

    }

    @Override
    protected void setup() {
        super.setup();
        System.out.println("[INFO] BlockchainServer alive: " + this.getAID());
        try {
            Path path = Paths.get(".").resolve(this.getLocalName() + "-blockchain");
            //System.out.println("[INFO] Reading file for: " + this.getLocalName());
            path.toFile().mkdir();
            bcHandler = new BlockchainHandler(path);
        } catch (IOException | ClassNotFoundException e) {
            System.out.println(Arrays.toString(e.getStackTrace()));
        }
        this.register("blockchain-server", "");
    }

    public void addBlock(Block block) throws IOException {
        bcHandler.addBlock(block);
    }

    public void sendInformRefBlocks(ACLMessage request, List<Block> blocks) throws IOException {
        ACLMessage informRefBlocks = request.createReply();
        informRefBlocks.setPerformative(ACLMessage.INFORM_REF);
        informRefBlocks.setContent(bcHandler.serializeBlockList(blocks));
        this.send(informRefBlocks);
    }

    public void handleBlockAdditionRequest(ACLMessage request) throws InterruptedException {
        Logger.info("Block addition request received.");
        boolean finish = false;
        while (!finish) {
            try {
                tryBlockAddition(request);
                finish = true;
                this.sendConfirmResponse(request, "OK");
            } catch (BlockConflictException e) {
                finish = false;
                Integer sleepingTime = randomGenerator.nextInt(5000);
                this.logInfo(
                        "Block conflict encountered during propagate. Retrying after "
                                + sleepingTime.toString() + " ms."
                );
                Thread.sleep(sleepingTime);
            } catch (Exception e) {
                String message = "Incorrect block: " + Arrays.toString(e.getStackTrace());
                this.sendFailureResponse(request, message);
                finish = true;
            }
        }
    }

    private void tryBlockAddition(ACLMessage request) throws IOException, ClassNotFoundException, InterruptedException {
        Block block = Block.deserialize(request.getContent());
        this.addBlock(block);
        ArrayList<Block> blocks = new ArrayList<Block>();
        blocks.add(block);
        propoagateBlock(blocks);
        this.bcHandler.persist();
        this.sendInformRefBlocks(
                request,
                blocks
        );
        informSubscribers(block);
    }

    public void propoagateBlock(List<Block> blocks) throws IOException, InterruptedException, BlockConflictException {
        ACLMessage propagate = new ConversationMessage(ACLMessage.PROPAGATE);
        propagate.setContent(Serializer.toString((Serializable) blocks));
        propagate.setProtocol("PropagateBlock");
        List<AID> agents = findAgentsOfService("blockchain-server", "");
        agents.remove(this.getAID());
        for (AID aid:agents) {
            propagate.addReceiver(aid);
            this.send(propagate);
            ACLMessage response = waitForResponse(propagate);
            if(response.getPerformative()==ACLMessage.FAILURE)
            {
                throw new BlockConflictException();
            }
            propagate.removeReceiver(aid);
        }
    }

    public void handleQueryRefBlocks(ACLMessage request){
        Logger.info("QueryRefBlocks received.");
        try{
            Filter filter = (Filter)Serializer.fromString(request.getContent());
            this.sendInformRefBlocks(
                    request,
                    this.bcHandler.getBlocks(filter)
            );
        }
        catch (Exception e){
            System.out.println(Arrays.toString(e.getStackTrace()));
            this.sendFailureResponse(request, e.getMessage());
        }
    }

    public void handlePropagateBlock(ACLMessage propagate) throws IOException, ClassNotFoundException {
        ACLMessage reply = propagate.createReply();
        try {
            List<Block> blocks = (List<Block>) Serializer.fromString(propagate.getContent());
            this.bcHandler.addExternalBlocks(blocks);
            this.bcHandler.persist();
            reply.setPerformative(ACLMessage.CONFIRM);
            for(Block block:blocks){
                this.informSubscribers(block);
            }
        }catch (BlockConflictException e){
            reply.setPerformative(ACLMessage.FAILURE);
        }
        this.send(reply);
    }

    public void handleSubscribeBlocks(ACLMessage request){
        Logger.info("SubscribeBlocks received.");
        try{
            Subscription subscription = new Subscription(request);
            for (Subscription sub:subscriptions) {
                if(sub.getSubscriber() == subscription.getSubscriber()){
                    subscriptions.remove(sub);
                    break;
                }
            }
            subscriptions.add(subscription);
            ACLMessage reply = request.createReply();
            reply.setPerformative(ACLMessage.CONFIRM);
            this.send(reply);
        }
        catch (Exception e){
            System.out.println(Arrays.toString(e.getStackTrace()));
            this.sendFailureResponse(request, e.getMessage());
        }
    }

    private void informSubscribers(Block block) throws IOException {
        for (Subscription subscription: subscriptions) {
            if(subscription.getFilter().check(block)){
                List<Block> toSend = new ArrayList<Block>();
                toSend.add(block);
                subscription.getRequest().setConversationId("-1");
                sendInformRefBlocks(subscription.getRequest(), toSend);
            }
        }
    }

}
