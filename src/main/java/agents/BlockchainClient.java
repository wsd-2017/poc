package agents;

import agents.utils.ConversationMessage;
import blockchain.Block;
import blockchain.BlockchainHandler;
import blockchain.Serializer;
import blockchain.filters.Filter;
import jade.core.AID;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.*;

import static jade.lang.acl.MessageTemplate.MatchPerformative;
import static jade.lang.acl.MessageTemplate.MatchProtocol;
import static java.nio.charset.StandardCharsets.UTF_8;

public class BlockchainClient extends ClassroomReservationSystemAgent {

    public Vector<ACLMessage> sentBlockAdditionRequests;

    private PublicKey publicKey;

    private PrivateKey privateKey;

    protected BlockchainHandler bcHandler;

    public PublicKey getPublicKey() {
        return publicKey;
    }
    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    @Override
    protected void setup() {
        super.setup();
        try {
            initBlockchainClient();
        } catch (IOException | ClassNotFoundException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public BlockchainClient() throws NoSuchAlgorithmException, NoSuchMethodException {
        super();
//        sentBlockAdditionRequests = new Vector<ACLMessage>();
        this.addNewMessageProcessor(
                MatchPerformative(ACLMessage.INFORM_REF),
                "handleInformRefBlocks"
        );
        this.addNewMessageProcessor(
                MessageTemplate.and(MatchPerformative(ACLMessage.FAILURE),MatchProtocol("BlockAddition")),
                "handleFailureAddBlocks"
        );
    }

    public Block createBlock(Map<String, String> data) throws SignatureException, NoSuchAlgorithmException, InvalidKeyException, IOException {
        Block block = new Block(this.getPublicKey().toString(), data);
        this.signBlock(block);
        return block;
    }

    private void signBlock(Block block) throws IOException, InvalidKeyException, NoSuchAlgorithmException, SignatureException {
        String dataToSign = block.getDataToSign();
        Signature privateSignature = Signature.getInstance("SHA256withRSA");
        privateSignature.initSign(this.privateKey);
        privateSignature.update(dataToSign.getBytes(UTF_8));
        byte[] signature = privateSignature.sign();
        block.setCreatorSignature(Base64.getEncoder().encodeToString(signature));
    }

    public static boolean verifyBlockSignature(Block block, PublicKey publicKey) throws Exception {
        String signedData = block.getDataToSign();
        Signature publicSignature = Signature.getInstance("SHA256withRSA");
        publicSignature.initVerify(publicKey);
        publicSignature.update(signedData.getBytes(UTF_8));
        byte[] signatureBytes = Base64.getDecoder().decode(block.getCreatorSignature());
        return publicSignature.verify(signatureBytes);
    }

    public void processBlockAddition(Block block) throws IOException, InterruptedException {
        ACLMessage request = requestBlockAddition(block);
        this.waitForResponse(request);
        this.logInfo("Block correctly added.");
    }

    protected ACLMessage requestBlockAddition(Block block) throws InterruptedException, IOException {
        ACLMessage request = this.sendBlockAdditionRequest(block);
        this.logInfo("Block addition requested.");
        return request;
    }

    public ACLMessage sendBlockAdditionRequest(Block block) throws IOException, InterruptedException {
        ACLMessage blockAdditionRequest = new ConversationMessage(ACLMessage.REQUEST);
        blockAdditionRequest.setContent(block.serialize());
        blockAdditionRequest.setProtocol("BlockAddition");
        sendToRandomBlockchainServer(blockAdditionRequest);
        return blockAdditionRequest;
    }

    // todo refactor query and subscribe to comply DRY rule
    public ACLMessage queryRefBlocks(Filter filter) throws IOException, InterruptedException {
        ACLMessage query = new ConversationMessage(ACLMessage.QUERY_REF);
        query.setContent(Serializer.toString((Serializable) filter));
        sendToRandomBlockchainServer(query);
        return query;
    }

    public ACLMessage processQueryRefBlocks(Filter filter) throws IOException, InterruptedException {
        return waitForResponse(queryRefBlocks(filter));
    }

    public ACLMessage subscribeBlocks(Filter filter) throws IOException, InterruptedException {
        ACLMessage subscribe = new ConversationMessage(ACLMessage.SUBSCRIBE);
        subscribe.setProtocol("SubscribeBlocks");
        subscribe.setContent(Serializer.toString((Serializable) filter));
        sendToRandomBlockchainServer(subscribe);
        return subscribe;
    }

    public void processBlocksSubscription(Filter filter) throws IOException, InterruptedException {
        waitForResponse(subscribeBlocks(filter));
    }

    public ACLMessage sendToRandomBlockchainServer(ACLMessage message) throws InterruptedException {
        AID blockchainServer = this.findRandomAgentOfService("blockchain-server", "");
        message.addReceiver(blockchainServer);
        this.send(message);
        return message;
    }

    public void handleInformRefBlocks(ACLMessage inform) {
        try {
            List<Block> blocks = bcHandler.deserializeBlockList(inform.getContent());
            this.bcHandler.addExternalBlocks(blocks);
        } catch (Exception e) {
            this.logError(
                    "BlockchainClient: problem during handling InformRefBlocks: " +
                            e.getMessage()
            );
        }
    }


    protected void removeRequestCorrespondingMessage(ACLMessage message){
        Vector<ACLMessage> requestList = this.sentBlockAdditionRequests;
        ACLMessage requestToRemove = null;
        for (ACLMessage request :requestList ) {
            if (Objects.equals(request.getConversationId(), message.getConversationId())) {
                requestToRemove = request;
                break;
            }
        }
        requestList.remove(requestToRemove);
    }

    public void handleFailureAddBlocks(ACLMessage failure){
        this.logInfo("Failed to process block addition.");
    }

    private Path getBcPath() {
        Path homePath = Paths.get(System.getProperty("user.home"));
        Path bcPath = homePath.resolve(".blockchain-" + this.getLocalName());
        if (Files.notExists(bcPath)) {
            bcPath.toFile().mkdir();
            System.out.println("[INFO] Blockchain directory created: " + bcPath.toString());
        } else {
            System.out.println("[INFO] Blockchain directory exists: " + bcPath.toString());
        }
        return bcPath;
    }


    private void initBlockchainClient() throws NoSuchAlgorithmException, IOException, ClassNotFoundException {
        Path bcPath = getBcPath();
        bcHandler = new BlockchainHandler(bcPath);
        Path publicKeyPath = bcPath.resolve("pubkey.pem");
        Path privateKeyPath = bcPath.resolve("prvkey.pem");

        if(Files.exists(publicKeyPath) && Files.exists(privateKeyPath)) {
            try {
                byte[] publicKeyBytes = Files.readAllBytes(publicKeyPath);
                byte[] privateKeyBytes = Files.readAllBytes(privateKeyPath);
                KeyFactory kf = KeyFactory.getInstance("RSA");
                privateKey = kf.generatePrivate(new PKCS8EncodedKeySpec(privateKeyBytes));
                System.out.println("[INFO] reading private key: success!");
                publicKey = kf.generatePublic(new X509EncodedKeySpec(publicKeyBytes));
                System.out.println("[INFO] reading public key: success!");
                
            } catch(IOException e) {
                System.err.println("[ERROR] No such file: " + publicKey);
            } catch(InvalidKeySpecException e) {
                System.err.println("[ERROR] InvalidKeySpecException ");
            }
        } else {
            KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
            generator.initialize(2048, new SecureRandom());
            KeyPair pair = generator.generateKeyPair();
            privateKey = pair.getPrivate();
            publicKey = pair.getPublic();

            // save as files
            try {
                FileOutputStream fos;

                fos = new FileOutputStream(publicKeyPath.toString());
                fos.write(publicKey.getEncoded());
                fos.close();

                fos = new FileOutputStream(privateKeyPath.toString());
                fos.write(privateKey.getEncoded());
                fos.close();

            } catch (IOException e) {
                System.err.println("file error");
            }
        }
    }

}
