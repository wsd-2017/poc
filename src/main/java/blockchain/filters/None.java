package blockchain.filters;

import blockchain.Block;

public class None implements Filter {

    @Override
    public boolean check(Block block) {
        return false;
    }
}
