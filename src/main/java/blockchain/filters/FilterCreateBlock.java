package blockchain.filters;

import blockchain.Block;

import java.io.Serializable;
import java.util.Map;

public class FilterCreateBlock extends AbstractFilter implements Filter, Serializable {

    @Override
    public boolean check(Block block) {
        Map<String, String> data = block.getData();
        return data.containsKey("SenderAID") &&
                data.containsKey("RoomID") &&
                data.containsKey("Capacity") &&
                data.containsKey("Tags");
    }
}
