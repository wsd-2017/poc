package blockchain.filters;

import blockchain.Block;

import java.util.Objects;

public class Creator implements Filter {


    private String creatorId;

    public Creator(String creatorId) {
        this.creatorId = creatorId;
    }

    @Override
    public boolean check(Block block) {
        return block.getCreatorId().equals(creatorId);
    }


}
