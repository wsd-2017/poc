package blockchain.filters;

import blockchain.BlockchainHandler;
import blockchain.Serializer;

import java.io.IOException;
import java.io.Serializable;

public abstract class AbstractFilter implements Filter, Serializable {

    public ComplexFilter or(AbstractFilter filter) throws IOException, ClassNotFoundException {
        AbstractFilter filter1Copy = copy(this);
        AbstractFilter filter2Copy = copy(filter);
        return new ComplexFilter(ComplexFilter.Type.OR, filter1Copy, filter2Copy);
    }

    public ComplexFilter and(AbstractFilter filter) throws IOException, ClassNotFoundException {
        AbstractFilter filter1Copy = copy(this);
        AbstractFilter filter2Copy = copy(filter);
        return new ComplexFilter(ComplexFilter.Type.AND, filter1Copy, filter2Copy);
    }

    private AbstractFilter copy(AbstractFilter filter) throws IOException, ClassNotFoundException {
        return (AbstractFilter)(Serializer.fromString(Serializer.toString((Serializable) filter)));
    }

}
