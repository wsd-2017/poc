package blockchain.filters;

import blockchain.Block;

public interface Filter {

    public boolean check(Block block);
}
