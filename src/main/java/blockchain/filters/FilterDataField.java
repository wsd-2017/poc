package blockchain.filters;

import blockchain.Block;

public class FilterDataField extends AbstractFilter {

    private String key;
    private String value;

    public FilterDataField(String key, String value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public boolean check(Block block) {
        if (block.getData().containsKey(key)) {
            return block.getData().get(key).equals(value);
        } else {
            return false;
        }
    }
}
