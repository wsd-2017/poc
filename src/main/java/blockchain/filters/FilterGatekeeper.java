package blockchain.filters;

import agents.Gatekeeper;
import blockchain.Block;

public class FilterGatekeeper extends AbstractFilter {

    private String gatekeeperName;

    public FilterGatekeeper(String gatekeeperName) {
        super();
        this.gatekeeperName = gatekeeperName;
    }

    @Override
    public boolean check(Block block) {
        if (block.getData().containsKey("GatekeeperAID")) {
            return block.getData().get("GatekeeperAID").equals(gatekeeperName);
        } else {
            return false;
        }
    }
}
