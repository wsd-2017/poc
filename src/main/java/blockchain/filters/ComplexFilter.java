package blockchain.filters;

import blockchain.Block;

public class ComplexFilter extends AbstractFilter {

    @Override
    public boolean check(Block block) {
        if (type == Type.AND){
            return checkAnd(block);
        }
        else{
            return checkOr(block);
        }
    }

    private boolean checkOr(Block block) {
        return (filter1.check(block) || filter2.check(block));
    }

    private boolean checkAnd(Block block) {
        return (filter1.check(block) && filter2.check(block));
    }

    public enum Type{AND, OR};
    private Type type;
    private Filter filter1;
    private Filter filter2;

    public ComplexFilter(Type type, Filter filter1, Filter filter2) {
        this.type = type;
        this.filter1 = filter1;
        this.filter2 = filter2;
    }
}
