package blockchain.filters;

import agents.utils.CommunicateName;
import blockchain.Block;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Map;

public class FilterBookingDate extends AbstractFilter implements Filter, Serializable {

    String duration;
    LocalDate time;
    String hour;

    public FilterBookingDate(LocalDate time, String hour, String duration) {
        this.duration = duration;
        this.time = time;
        this.hour = hour;
    }

    @Override
    public boolean check(Block block) {
        Map<String, String> data = block.getData();
        return data.containsKey("CommunicateName") &&
                (data.get("CommunicateName").equals(CommunicateName.BOOK_CLASSROOM) ||
                        data.get("CommunicateName").equals(CommunicateName.CANCEL_BOOKING)) &&
                isBookedAtThisDay(data) &&
                isBookedAtThisHour(data);
    }

    Boolean isBookedAtThisDay(Map<String, String> data){
        return data.containsKey("Date") && data.get("Date").equals(time.toString());
    }

    //czy rezerwacje nie koliduja ze soba czasowo
    Boolean isBookedAtThisHour(Map<String, String> data){
        if(!data.containsKey("Hour") || !data.containsKey("Duration")){
            return false;
        }
        Integer blockStart = Integer.valueOf(data.get("Hour"));
        Integer blockEnd = blockStart + Integer.valueOf(data.get("Duration"));

        Integer myStart =  Integer.valueOf(this.hour);
        Integer myEnd =  myStart + Integer.valueOf(this.duration);

        if(myStart >= blockStart &&
                blockEnd > myStart){
            return true;
        }
        else if(blockStart < myEnd &&
                blockStart >= myStart){
            return true;
        }
        else {
            return false;
        }
    }
}
