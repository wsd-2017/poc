package blockchain.filters;

import blockchain.Block;
import blockchain.blocks.BookClassroom;
import blockchain.blocks.CancelBooking;
import blockchain.blocks.Resign;

import java.io.Serializable;
import java.util.Map;

public class FilterUserBooking extends AbstractFilter implements Filter, Serializable {

    String agent;

    public FilterUserBooking(String agent) {
        super();
        this.agent = agent;
    }

    @Override
    public boolean check(Block block) {
        Map<String, String> data = block.getData();
        return (block instanceof BookClassroom && isMyBooking(data)) ||
                (block instanceof CancelBooking && isMyBooking(data)) ;
    }

    Boolean isMyBooking(Map<String, String> data){
        return data.containsKey("SenderAID") && data.get("SenderAID").equals(agent);
    }
}
