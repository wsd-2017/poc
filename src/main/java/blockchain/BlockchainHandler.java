package blockchain;

import blockchain.filters.AbstractFilter;
import blockchain.filters.Filter;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class BlockchainHandler {

    // Let's take a few assumptions:
    // 1. all blocks are stored in the same directory
    // 2. each block is stored in one file
    // 3. every filename is in format {id}_{hash}
    // 4. id is an unique unsigned integer and the index of the block on the list
    // 5. hash is a  sha256 hash of the block in hex
    // 6. each block is stored as its serialized form (result of the Block.serialize() method).

    private Path storeDirectory;
    private List<Block> blocks;

    public BlockchainHandler(Path storeDirectory) throws IOException, ClassNotFoundException {
        this.storeDirectory = storeDirectory;
        this.loadBlocksFromDirectory(storeDirectory);
    }

    private void loadBlocksFromDirectory(Path storeDirectory) throws IOException, ClassNotFoundException {
        blocks = new ArrayList<Block>();
        File[] files = getFilesListInDirectory(storeDirectory);
        for (File file : files) {
            if (file.getName().matches("^[0-9]+_[0-9A-Fa-f]+$")) {
                String serializedBlock = new String(Files.readAllBytes(file.toPath()));
                Block block = Block.deserialize(serializedBlock);
                Integer id = Integer.decode(file.getName().split("_")[0]);
                block.setBlockId(id);
                block.setPersisted(true);
                if(id < blocks.size()){
                    blocks.add(id, block);
                }
                else {
                    blocks.add(block);
                }
            }
        }
    }

    private File[] getFilesListInDirectory(Path path) {
        File folder = path.toFile();
        File[] listOfFiles = folder.listFiles();

        for (File listOfFile : listOfFiles) {
            if (listOfFile.isFile()) {
                System.out.println("File " + listOfFile.getName());
            } else if (listOfFile.isDirectory()) {
                System.out.println("Directory " + listOfFile.getName());
            }
        }
        return listOfFiles;
    }

    private String difficultyRegex(int difficulty) {
        return "^[0]{" + Integer.toString(difficulty) + "}.*$";
    }

    public Block mine(Block block, int difficulty) throws IOException {
        boolean mined = false;
        Long nonce = 0L;
        while (!mined) {
            nonce++;
            block.setNonce(nonce.toString());
            if (checkMined(block, difficulty)) {
                mined = true;
            }
        }
        return block;
    }

    public List<Block> getBlocks(Filter filter) {
        List<Block> result = new ArrayList<>();
        for (Block block : blocks) {
            if (filter.check(block)) {
                result.add(block);
            }
        }
        return result;
    }

    public void addBlock(Block block) throws IOException {
        String previousBlockHash = "";
        if (blocks.size() > 0) {
            previousBlockHash = blocks.get(blocks.size() - 1).getHash();
        }
        block.setPreviousBlockHash(previousBlockHash);
        int id = blocks.size();
        mine(block, calculateDifficulty());
        block.setBlockId(id);
        blocks.add(block);
        //saveToFile(block, id);
    }

    private void saveToFile(Block block, Integer id) throws IOException {
        String filename = id.toString() + "_" + block.getHash();
        PrintWriter writer = new PrintWriter(storeDirectory.resolve(filename).toString(), "UTF-8");
        System.out.println("BLOCK SERIALIZE: " + block.serialize());
        writer.write(block.serialize());
        writer.flush();
        writer.close();
    }

    private int calculateDifficulty() {
        return 3;
    }

    public boolean checkMined(Block block, int difficulty) throws IOException {
        String hash = block.getHash();
        String regex = difficultyRegex(difficulty);
        return hash.matches(regex);
    }

    public List<Block> getAllBlocks() {
        return blocks;
    }

    public String serializeBlockList(List<Block> blocks) throws IOException {
        return Serializer.toString((Serializable) blocks);
    }

    public List<Block> deserializeBlockList(String serializedBlocks) throws IOException, ClassNotFoundException {
        return (List<Block>) Serializer.fromString(serializedBlocks);
    }

    public void addExternalBlocks(List<Block> externalBlocks) throws BlockConflictException {
        this.removeDuplicates(externalBlocks);
        this.assertNoDuplicates(externalBlocks);
        this.blocks.addAll(externalBlocks);
    }

    public void removeDuplicates(List<Block> otherBlocks) {
        List<Block> toRemove = new ArrayList<Block>();
        for (Block other : otherBlocks) {
            for (Block current : this.blocks) {
                if (current.equals(other)) {
                    toRemove.add(other);
                }
            }
        }
        otherBlocks.removeAll(toRemove);
    }

    public void assertNoDuplicates(List<Block> otherBlocks) throws BlockConflictException {
        for (Block other : otherBlocks) {
            for (Block current : this.blocks) {
                if (other.getPreviousBlockHash().equals(current.getPreviousBlockHash())) {
                    throw new BlockConflictException();
                }
            }
        }
    }

    public void persist() throws IOException {
        for (Block block: this.blocks) {
            if(!block.isPersisted()){
                saveToFile(block, block.getBlockId());
                block.setPersisted(true);
            }
        }
    }

    public void removeNewestBlocks(){
        this.blocks.remove(blocks.size()-1);
    }
}
