package blockchain;

public class BlockConflictException extends RuntimeException {
    public BlockConflictException() {
        super("Operation lead to conflict in blockchain.");
    }
}
