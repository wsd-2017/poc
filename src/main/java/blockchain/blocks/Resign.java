package blockchain.blocks;

import blockchain.Block;

import java.io.IOException;
import java.util.HashMap;

public class Resign extends Block {
    public Resign(String creatorId, String roomId, String bookId) throws IOException {
        super(creatorId, new HashMap<>());
        this.data.put("CommunicateName", "Resign");
        this.data.put("SenderAID", creatorId);
        this.data.put("RoomID", roomId);
        this.data.put("GatekeeperAID", bookId);
    }
}