package blockchain.blocks;

import agents.utils.Permission;
import blockchain.Block;

import java.io.IOException;
import java.util.HashMap;

public class AssignPermission extends Block {
    public AssignPermission(String creatorId, String roomId, String userId, Permission permission, Integer priority) throws IOException {
        super(creatorId, new HashMap<>());
        this.data.put("CommunicateName", "AssignPermission");
        this.data.put("SenderAID", creatorId);
        this.data.put("RoomID", roomId);
        this.data.put("ClassUserAID", userId);
        this.data.put("Permission", permission.toString());
        this.data.put("Priority", priority.toString());
    }
}
