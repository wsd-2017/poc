package blockchain.blocks;

import blockchain.Block;

import java.io.IOException;
import java.util.HashMap;

public class RevokeOwner extends Block {
    public RevokeOwner(String creatorId, String roomId, String ownerID) throws IOException {
        super(creatorId, new HashMap<>());
        this.data.put("CommunicateName", "RevokeOwner");
        this.data.put("SenderAID", creatorId);
        this.data.put("RoomID", roomId);
        this.data.put("OwnerAID", ownerID);
    }
}
