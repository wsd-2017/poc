package blockchain.blocks;

import blockchain.Block;

import java.io.IOException;
import java.util.HashMap;

public class RevokeGatekeeper extends Block{
    public RevokeGatekeeper(String creatorId, String roomId, String gatekeeperAID) throws IOException {
        super(creatorId, new HashMap<>());
        this.data.put("CommunicateName", "RevokeGatekeeper");
        this.data.put("SenderAID", creatorId);
        this.data.put("RoomID", roomId);
        this.data.put("GatekeeperAID", gatekeeperAID);
    }
}
