package blockchain.blocks;

import blockchain.Block;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class AssignGatekeeper extends Block{

    public AssignGatekeeper(String creatorId, String roomId, String gatekeeperAID) throws IOException {
        super(creatorId, new HashMap<>());
        this.data.put("CommunicateName", "AssignGatekeeper");
        this.data.put("SenderAID", creatorId);
        this.data.put("RoomID", roomId);
        this.data.put("GatekeeperAID", gatekeeperAID);
    }
}
