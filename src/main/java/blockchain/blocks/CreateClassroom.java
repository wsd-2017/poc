package blockchain.blocks;

import blockchain.Block;
import jade.core.AID;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CreateClassroom extends Block {
    public CreateClassroom(String creatorId, String roomId, Integer capacity, String tags) throws IOException {
        super(creatorId, new HashMap<>());
        this.data.put("CommunicateName", "CreateClassroom");
        this.data.put("SenderAID", creatorId);
        this.data.put("RoomID", roomId);
        this.data.put("Capacity", capacity.toString());
        this.data.put("Tags", tags);
    }

    public String getSenderAID(){
        return data.get("SenderAID");
    }



}
