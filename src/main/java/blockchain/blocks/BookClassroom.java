package blockchain.blocks;

import agents.utils.CommunicateName;
import blockchain.Block;

import java.io.IOException;
import java.time.LocalDate;
import java.util.HashMap;

public class BookClassroom extends Block {
    public BookClassroom(String creatorId, String roomId, LocalDate time, String hour, Integer duration) throws IOException {
        super(creatorId, new HashMap<>());
        this.data.put("CommunicateName", CommunicateName.BOOK_CLASSROOM);
        this.data.put("SenderAID", creatorId);
        this.data.put("RoomID", roomId);
        this.data.put("Date", time.toString());
        this.data.put("Hour", hour);
        this.data.put("Duration", duration.toString());
    }
}
