package blockchain.blocks;

import agents.utils.CommunicateName;
import blockchain.Block;

import java.io.IOException;
import java.util.HashMap;

public class AssignOwner extends Block {
    public AssignOwner(String creatorId, String roomId, String ownerID) throws IOException {
        super(creatorId, new HashMap<>());
        this.data.put("CommunicateName", CommunicateName.ASSIGN_OWNER);
        this.data.put("SenderAID", creatorId);
        this.data.put("RoomID", roomId);
        this.data.put("OwnerAID", ownerID);
    }
}
