package blockchain;

import org.apache.commons.codec.digest.DigestUtils;

import java.io.IOException;
import java.io.Serializable;
import java.util.*;

public class Block implements Serializable {
    private Date creatingTimestamp;
    private String creatorId;
    protected Map<String, String> data;
    private String creatorSignature;
    private String minerId;
    private String previousBlockHash;
    private String nonce;
    private int randomField;
    static private Random randomGenerator = new Random();
    private String dataId;

    // Fields blockId and persiseted are not affected during hashing and comparision
    // Created to simplify BlockchainHandler implementation
    private Integer blockId;
    private boolean persisted;


    public Block(String creatorId, Map<String, String> data) throws IOException {
        this.creatorId = creatorId;

        this.data = data;
        this.creatingTimestamp = new Date();
        this.randomField = randomGenerator.nextInt();
        this.dataId = DigestUtils.shaHex(getDataToSign());
    }

    static public Block deserialize(String serializedBlock) throws IOException, ClassNotFoundException {
        return (Block)Serializer.fromString(serializedBlock);
    }

    public String getDataToSign() throws IOException {
        List<Object> objects = new ArrayList<Object>();
        objects.add(this.creatingTimestamp);
        objects.add(this.creatorId);
        objects.add(this.data);
        objects.add(this.randomField);
        return Serializer.toString((Serializable) objects);
    }

    public String getHash() throws IOException {
        String dataToHash = this.serialize();
        return DigestUtils.shaHex(dataToHash);
    }

    public String serialize() throws IOException {
        return Serializer.toString( this);
    }

    public Date getCreatingTimestamp() {
        return creatingTimestamp;
    }


    public String getCreatorId() {
        return creatorId;
    }

    public Map<String, String> getData() {
        return data;
    }

    public String getCreatorSignature() {
        return creatorSignature;
    }

    public void setCreatorSignature(String creatorSignature) {
        this.creatorSignature = creatorSignature;
    }

    public String getMinerId() {
        return minerId;
    }

    public void setMinerId(String minerId) {
        this.minerId = minerId;
    }

    public String getPreviousBlockHash() {
        return previousBlockHash;
    }

    public void setPreviousBlockHash(String previousBlockHash) {
        this.previousBlockHash = previousBlockHash;
    }

    public String getNonce() {
        return nonce;
    }

    public void setNonce(String nonce) {
        this.nonce = nonce;
    }

    public Integer getBlockId() {
        return blockId;
    }

    public void setBlockId(Integer blockId) {
        this.blockId = blockId;
    }

    public boolean isPersisted() {
        return persisted;
    }

    public void setPersisted(boolean persisted) {
        this.persisted = persisted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Block block = (Block) o;

        if (randomField != block.randomField) return false;
        if (creatingTimestamp != null ? !creatingTimestamp.equals(block.creatingTimestamp) : block.creatingTimestamp != null)
            return false;
        if (creatorId != null ? !creatorId.equals(block.creatorId) : block.creatorId != null) return false;
        if (data != null ? !data.equals(block.data) : block.data != null) return false;
        if (creatorSignature != null ? !creatorSignature.equals(block.creatorSignature) : block.creatorSignature != null)
            return false;
        if (minerId != null ? !minerId.equals(block.minerId) : block.minerId != null) return false;
        if (previousBlockHash != null ? !previousBlockHash.equals(block.previousBlockHash) : block.previousBlockHash != null)
            return false;
        if (nonce != null ? !nonce.equals(block.nonce) : block.nonce != null) return false;
        return dataId != null ? dataId.equals(block.dataId) : block.dataId == null;
    }

    @Override
    public int hashCode() {
        int result = creatingTimestamp != null ? creatingTimestamp.hashCode() : 0;
        result = 31 * result + (creatorId != null ? creatorId.hashCode() : 0);
        result = 31 * result + (data != null ? data.hashCode() : 0);
        result = 31 * result + (creatorSignature != null ? creatorSignature.hashCode() : 0);
        result = 31 * result + (minerId != null ? minerId.hashCode() : 0);
        result = 31 * result + (previousBlockHash != null ? previousBlockHash.hashCode() : 0);
        result = 31 * result + (nonce != null ? nonce.hashCode() : 0);
        result = 31 * result + randomField;
        result = 31 * result + (dataId != null ? dataId.hashCode() : 0);
        return result;
    }

}
