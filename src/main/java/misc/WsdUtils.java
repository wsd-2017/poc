package misc;

import java.util.Map;

/**
 * Created by Radek on 2018-01-16.
 */
public class WsdUtils {
    public static boolean isAnyNullOrEmpty(String ... strings){
        for(String s: strings){
            if(s == null || s.isEmpty()){
                return true;
            }
        }
        return false;
    }

    public static boolean isAnyNullOrEmpty(Object ... objects){
        for(Object o: objects){
            if(o == null || o.toString().isEmpty()){
                return true;
            }
        }
        return false;
    }

    public static boolean valueExistsAndEquals(Map<String, String> data, String key, String toCompare){
        return data.containsKey(key) && data.get(key).equals(toCompare);
    }

}
