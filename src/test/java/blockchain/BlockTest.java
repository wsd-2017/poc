package blockchain;

import junit.framework.TestCase;
import org.unitils.reflectionassert.ReflectionAssert;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

public class BlockTest extends TestCase {
    public void testGetDataToSign() throws Exception {
        // Given
        Map<String, String> data = new HashMap<String, String>();
        Block block = new Block("1", data);

        // When
        String dataToSign = block.getDataToSign();

        // Then
        assertNotNull(dataToSign);
    }

    public void testGetHash() throws IOException {
        // Given
        Map<String, String> data = new HashMap<String, String>();
        Block block = new Block("1", data);

        // When
        String hash1 = block.getHash();
        block.setNonce("something");
        String hash2 = block.getHash();

        // Then
        assertNotNull(hash1);
        assertNotNull(hash2);
        assertNotSame(hash1, hash2);
    }

    public void testCreateBlock() throws IOException {
        // Given
        Map<String, String> data = new HashMap<String, String>();
        Date justBeforeCreating = new Date();

        // When
        Block block = new Block("1", data);

        // Then
        Date creatingDate = block.getCreatingTimestamp();
        assertTrue(creatingDate.equals(justBeforeCreating)); // TODO find better way of testing it.
    }

    public void testSerialization() throws IOException, ClassNotFoundException {
        // Given
        Map<String, String> data = new HashMap<String, String>();
        Block block = new Block("1", data);

        // When
        String serlialized = block.serialize();
        Block deserialized = Block.deserialize(serlialized);

        // Then
        ReflectionAssert.assertReflectionEquals(block, deserialized);
    }
}