package blockchain;

import junit.framework.TestCase;
import org.junit.rules.TemporaryFolder;
import org.unitils.thirdparty.org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BlockchainHandlerTest extends TestCase {

    private Path path;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        path = Paths.get(".").resolve("blockchain");
        if( path.toFile().exists()) {
            FileUtils.deleteDirectory(path.toFile());
        }
        path.toFile().mkdir();
    }

    public void testMine() throws Exception {
        // Given
        BlockchainHandler handler = new BlockchainHandler(path);
        Map<String, String> data = new HashMap<>();
        data.put("key1", "value1");
        Block block = new Block("creator", data);

        // When
        block = handler.mine(block, 3);

        // Then
        assertTrue(block.getHash().matches("^[0]{3}.*$"));
    }

    public void testCheckMinedOnMined() throws IOException, ClassNotFoundException {
        // Givrn
        BlockchainHandler handler = new BlockchainHandler(path);
        Map<String, String> data = new HashMap<>();
        data.put("key1", "value1");
        Block block = new Block("creator", data);
        handler.mine(block, 3);

        // When
        boolean isMined = handler.checkMined(block, 3);

        // Then
        assertTrue(isMined);
    }


    public void testCheckMinedOnNotMined() throws IOException, ClassNotFoundException {
        // Givrn
        BlockchainHandler handler = new BlockchainHandler(path);
        Map<String, String> data = new HashMap<>();
        data.put("key1", "value1");
        Block block = new Block("creator", data);

        // When
        boolean isMined = handler.checkMined(block, 3);

        // Then
        assertFalse(isMined);
    }

    public void testGetAllBlocks() throws IOException, ClassNotFoundException {
        // Givrn
        BlockchainHandler handler = new BlockchainHandler(path);
        Map<String, String> data = new HashMap<>();
        data.put("key1", "value1");
        Block block1 = new Block("creator", data);
        Block block2 = new Block("creator", data);
        Block block3 = new Block("creator", data);

        // When
        handler.addBlock(block1);
        handler.addBlock(block2);
        handler.addBlock(block3);
        List<Block> allBlocks = handler.getAllBlocks();

        // Then
        assertEquals(3, allBlocks.size());
        assertEquals(block1, allBlocks.get(0));
        assertEquals(block2, allBlocks.get(1));
        assertEquals(block3, allBlocks.get(2));
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        if( path.toFile().exists()) {
            FileUtils.deleteDirectory(path.toFile());
        }
    }
}