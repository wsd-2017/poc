package agents;

import blockchain.Block;
import jade.wrapper.AgentController;
import junit.framework.TestCase;

import java.util.HashMap;
import java.util.Map;

public class BlockchainClientTest extends TestCase {

    public void testCreateBlock() throws Exception {
        // Given
        Map<String, String> data = new HashMap<>();
        data.put("key1", "value1");
        data.put("key2", "value2");
        data.put("key3", "value3");
        BlockchainClient agent = new BlockchainClient();
        agent.setup();

        // When
        Block block = agent.createBlock(data);

        // Then
        assertTrue( BlockchainClient.verifyBlockSignature(block, agent.getPublicKey()));
        assertEquals(block.getData().get("key1"), "value1");
        assertEquals(block.getData().get("key2"), "value2");
        assertEquals(block.getData().get("key3"), "value3");
    }

}